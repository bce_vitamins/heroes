#DROP DATABASE thronebase;
CREATE DATABASE thronebase;
USE thronebase;

create table articles
(
    id          bigint       not null auto_increment,
    date_time   varchar(255) not null,
    description TEXT         not null,
    foreword    TEXT         not null,
    title       varchar(255) not null,
    primary key (id)
);

create table battles
(
    id          bigint not null auto_increment,
    date_time   TEXT   not null,
    description TEXT   not null,
    title       varchar(255),
    primary key (id)
);

create table characteristics
(
    id       bigint  not null auto_increment,
    agility  integer not null,
    health   integer not null,
    strength integer not null,
    hero_id  bigint,
    primary key (id)
);

create table comments
(
    id                bigint       not null auto_increment,
    comment           TEXT         not null,
    comment_date_time varchar(255) not null,
    article_id        bigint,
    user_id           bigint,
    primary key (id)
);

create table heroes
(
    id          bigint       not null auto_increment,
    age         integer      not null,
    description TEXT         not null,
    gender      varchar(255) not null,
    name        varchar(255) not null,
    nickname    varchar(255),
    primary key (id)
);

create table participants
(
    battle_id bigint not null,
    hero_id   bigint not null,
    primary key (battle_id, hero_id)
);

create table roles
(
    id   bigint       not null auto_increment,
    name varchar(255) not null,
    primary key (id)
);

create table users
(
    id       bigint       not null auto_increment,
    login    varchar(255) not null,
    password varchar(64)  not null,
    primary key (id)
);

create table users_roles
(
    user_id bigint not null,
    role_id bigint not null
);

alter table users
    add constraint UK_ow0gan20590jrb00upg3va2fn unique (login);

alter table characteristics
    add constraint FKriceyfnclpdx2902c2daes0o7
        foreign key (hero_id)
            references heroes (id);

alter table comments
    add constraint FKk4ib6syde10dalk7r7xdl0m5p
        foreign key (article_id)
            references articles (id);

alter table comments
    add constraint FK8omq0tc18jd43bu5tjh6jvraq
        foreign key (user_id)
            references users (id);

alter table participants
    add constraint FKscg3o7ks2w1o2jnx1gnj38pvq
        foreign key (hero_id)
            references heroes (id);

alter table participants
    add constraint FK4o0a3r8apd4ercwicv25w8rtj
        foreign key (battle_id)
            references battles (id);

alter table users_roles
    add constraint FKj6m8fwv7oqv74fcehir1a9ffy
        foreign key (role_id)
            references roles (id);

alter table users_roles
    add constraint FK2o0jvgh89lemvvo17cbqvdxaa
        foreign key (user_id)
            references users (id);

INSERT INTO articles (date_time, description, foreword, title) VALUES ('2019-12-23T01:38:49.368', 'Сам текст1', 'Джордж Мартин сейчас в поездке по острову Ирландия: в Дублине только что закончился 77-й Worldcon, а в Белфасте — уже в Северной Ирландии — проходит смежный EuroCon (TitanCon 2019). Неудивительно, что писатель воспользовался возможностью в очередной раз съездить в Ирландию и посетить оба конвента как почетный гость. Пригласили его и в Ирландский кинематографический институт (IFI) на специальный показ фильма «Запретная планета» (1956) — одного из любимых у Мартина. Здесь сайту JOE.ie удалось побеседовать с Мартином о процессе работы над книгами, сериалах и многом другом.', 'Интервью Джорджа Мартина');
INSERT INTO articles (date_time, description, foreword, title) VALUES ('2019-12-23T01:38:49.368', 'Сам текст1', 'Джордж Мартин сейчас в поездке по острову Ирландия: в Дублине только что закончился 77-й Worldcon, а в Белфасте — уже в Северной Ирландии — проходит смежный EuroCon (TitanCon 2019). Неудивительно, что писатель воспользовался возможностью в очередной раз съездить в Ирландию и посетить оба конвента как почетный гость. Пригласили его и в Ирландский кинематографический институт (IFI) на специальный показ фильма «Запретная планета» (1956) — одного из любимых у Мартина. Здесь сайту JOE.ie удалось побеседовать с Мартином о процессе работы над книгами, сериалах и многом другом.', 'Интервью Джорджа Мартина');
INSERT INTO articles (date_time, description, foreword, title) VALUES ('2019-12-23T01:38:49.368', 'Сам текст1', 'Джордж Мартин сейчас в поездке по острову Ирландия: в Дублине только что закончился 77-й Worldcon, а в Белфасте — уже в Северной Ирландии — проходит смежный EuroCon (TitanCon 2019). Неудивительно, что писатель воспользовался возможностью в очередной раз съездить в Ирландию и посетить оба конвента как почетный гость. Пригласили его и в Ирландский кинематографический институт (IFI) на специальный показ фильма «Запретная планета» (1956) — одного из любимых у Мартина. Здесь сайту JOE.ie удалось побеседовать с Мартином о процессе работы над книгами, сериалах и многом другом.', 'Интервью Джорджа Мартина');
INSERT INTO articles (date_time, description, foreword, title) VALUES ('2019-12-23T01:38:49.368', 'Сам текст2', 'История небольшой семейной компании Shire Post Mint началась с покупки в 2001 году антикварного пресса для чеканки монет. После нескольких лет испытаний основатель компании Том Маринер получил свою первую лицензию — несколько отчеканенных им монет так понравились Джорджу Мартину, что он дал добро на использование своего мира. Теперь в коллекции компании монеты по книгам Джона Толкина, Роберта Джордана, Брэндона Сандерсона, Патрика Ротфусса, Питера В. Бретта, Роберта Говарда, несколько исторических монет, но коллекция по «Песни Льда и Пламени» остается самой большой — около 50 различных вариантов. Во времена до сериала эти монеты были единственной коллекционной штукой для фанатов помимо книг. У нас, например, есть монета Валар Моргулис, подаренная самим Джорджем Мартином.', 'Монеты Вестероса и Эссоса');
INSERT INTO articles (date_time, description, foreword, title) VALUES ('2019-12-23T01:38:49.368', 'Сам текст2', 'История небольшой семейной компании Shire Post Mint началась с покупки в 2001 году антикварного пресса для чеканки монет. После нескольких лет испытаний основатель компании Том Маринер получил свою первую лицензию — несколько отчеканенных им монет так понравились Джорджу Мартину, что он дал добро на использование своего мира. Теперь в коллекции компании монеты по книгам Джона Толкина, Роберта Джордана, Брэндона Сандерсона, Патрика Ротфусса, Питера В. Бретта, Роберта Говарда, несколько исторических монет, но коллекция по «Песни Льда и Пламени» остается самой большой — около 50 различных вариантов. Во времена до сериала эти монеты были единственной коллекционной штукой для фанатов помимо книг. У нас, например, есть монета Валар Моргулис, подаренная самим Джорджем Мартином.', 'Монеты Вестероса и Эссоса');
INSERT INTO articles (date_time, description, foreword, title) VALUES ('2019-12-23T01:38:49.368', 'Сам текст2', 'История небольшой семейной компании Shire Post Mint началась с покупки в 2001 году антикварного пресса для чеканки монет. После нескольких лет испытаний основатель компании Том Маринер получил свою первую лицензию — несколько отчеканенных им монет так понравились Джорджу Мартину, что он дал добро на использование своего мира. Теперь в коллекции компании монеты по книгам Джона Толкина, Роберта Джордана, Брэндона Сандерсона, Патрика Ротфусса, Питера В. Бретта, Роберта Говарда, несколько исторических монет, но коллекция по «Песни Льда и Пламени» остается самой большой — около 50 различных вариантов. Во времена до сериала эти монеты были единственной коллекционной штукой для фанатов помимо книг. У нас, например, есть монета Валар Моргулис, подаренная самим Джорджем Мартином.', 'Монеты Вестероса и Эссоса');
INSERT INTO articles (date_time, description, foreword, title) VALUES ('2019-12-23T01:38:49.368', 'Сам текст2', 'История небольшой семейной компании Shire Post Mint началась с покупки в 2001 году антикварного пресса для чеканки монет. После нескольких лет испытаний основатель компании Том Маринер получил свою первую лицензию — несколько отчеканенных им монет так понравились Джорджу Мартину, что он дал добро на использование своего мира. Теперь в коллекции компании монеты по книгам Джона Толкина, Роберта Джордана, Брэндона Сандерсона, Патрика Ротфусса, Питера В. Бретта, Роберта Говарда, несколько исторических монет, но коллекция по «Песни Льда и Пламени» остается самой большой — около 50 различных вариантов. Во времена до сериала эти монеты были единственной коллекционной штукой для фанатов помимо книг. У нас, например, есть монета Валар Моргулис, подаренная самим Джорджем Мартином.', 'Монеты Вестероса и Эссоса');
INSERT INTO articles (date_time, description, foreword, title) VALUES ('2019-12-23T01:38:49.368', 'Сам текст2', 'История небольшой семейной компании Shire Post Mint началась с покупки в 2001 году антикварного пресса для чеканки монет. После нескольких лет испытаний основатель компании Том Маринер получил свою первую лицензию — несколько отчеканенных им монет так понравились Джорджу Мартину, что он дал добро на использование своего мира. Теперь в коллекции компании монеты по книгам Джона Толкина, Роберта Джордана, Брэндона Сандерсона, Патрика Ротфусса, Питера В. Бретта, Роберта Говарда, несколько исторических монет, но коллекция по «Песни Льда и Пламени» остается самой большой — около 50 различных вариантов. Во времена до сериала эти монеты были единственной коллекционной штукой для фанатов помимо книг. У нас, например, есть монета Валар Моргулис, подаренная самим Джорджем Мартином.', 'Монеты Вестероса и Эссоса');

INSERT INTO battles (date_time, description, title) VALUES ('2019-12-23T01:38:49.368', 'Битва 1', 'Битва 1');
INSERT INTO battles (date_time, description, title) VALUES ('2019-12-23T01:38:49.368', 'Битва 2', 'Битва 2');
INSERT INTO battles (date_time, description, title) VALUES ('2019-12-23T01:38:49.368', 'Битва 3', 'Битва 3');
INSERT INTO battles (date_time, description, title) VALUES ('2019-12-23T01:38:49.368', 'Битва 4', 'Битва 4');
INSERT INTO battles (date_time, description, title) VALUES ('2019-12-23T01:38:49.368', 'Битва 5', 'Битва 5');
INSERT INTO battles (date_time, description, title) VALUES ('2019-12-23T01:38:49.368', 'Битва 6', 'Битва 6');
INSERT INTO battles (date_time, description, title) VALUES ('2019-12-23T01:38:49.368', 'Битва 7', 'Битва 7');
INSERT INTO battles (date_time, description, title) VALUES ('2019-12-23T01:38:49.368', 'Битва 8', 'Битва 8');

INSERT INTO heroes (age, description, gender, name, nickname) VALUES (40, 'Cолдат на службе Тайвина Ланнистера', 'MALE', 'Григор Клиган', 'Скачущая Гора');
INSERT INTO heroes (age, description, gender, name, nickname) VALUES (65, 'Глава дома Ланнистер', 'MALE', 'Тайвин Ланнистер', 'Лев Запада');
INSERT INTO heroes (age, description, gender, name, nickname) VALUES (35, 'Королевский гвардеец', 'MALE', 'Джейме Ланнистер', 'Цареубийца');
INSERT INTO heroes (age, description, gender, name, nickname) VALUES (36, 'Хранитель Севера', 'MALE', 'Эддард Старк', 'Тихий Волк');
INSERT INTO heroes (age, description, gender, name, nickname) VALUES (15, 'Незаконнорожденный сын Эддарда Старка', 'MALE', 'Джон Сноу', 'Бастард из Винтерфелла');
INSERT INTO heroes (age, description, gender, name, nickname) VALUES (35, 'Королева Вестероса', 'FEMALE', 'Серсея Ланнистер', '');
INSERT INTO heroes (age, description, gender, name, nickname) VALUES (12, 'Истинная королева Вестероса', 'FEMALE', 'Дейнерис Таргариен', 'Бурерожденная');
INSERT INTO heroes (age, description, gender, name, nickname) VALUES (112, 'Дракон Эйгона 1', 'FEMALE', 'Балерион', 'Ужасный');

INSERT INTO characteristics (agility, health, strength, hero_id) VALUES (100, 100, 50, 1);
INSERT INTO characteristics (agility, health, strength, hero_id) VALUES (50, 50, 50, 2);
INSERT INTO characteristics (agility, health, strength, hero_id) VALUES (50, 50, 50, 3);
INSERT INTO characteristics (agility, health, strength, hero_id) VALUES (50, 50, 50, 4);
INSERT INTO characteristics (agility, health, strength, hero_id) VALUES (50, 50, 50, 5);
INSERT INTO characteristics (agility, health, strength, hero_id) VALUES (50, 50, 50, 6);
INSERT INTO characteristics (agility, health, strength, hero_id) VALUES (50, 50, 50, 7);
INSERT INTO characteristics (agility, health, strength, hero_id) VALUES (50, 50, 50, 8);

INSERT INTO participants (battle_id, hero_id) VALUES (1, 1);
INSERT INTO participants (battle_id, hero_id) VALUES (2, 2);
INSERT INTO participants (battle_id, hero_id) VALUES (3, 3);
INSERT INTO participants (battle_id, hero_id) VALUES (4, 4);
INSERT INTO participants (battle_id, hero_id) VALUES (5, 5);
INSERT INTO participants (battle_id, hero_id) VALUES (6, 6);
INSERT INTO participants (battle_id, hero_id) VALUES (7, 7);
INSERT INTO participants (battle_id, hero_id) VALUES (8, 8);

INSERT INTO users (login, password) VALUES ('admin@throne.com', '$2a$10$QlYTH3iFqYFEp9OZWfL9ZuhGvCOlImZkSY8W8oY/yt3u434ENdpDq');
INSERT INTO users (login, password) VALUES ('editor1@throne.com', '$2a$10$QlYTH3iFqYFEp9OZWfL9ZuhGvCOlImZkSY8W8oY/yt3u434ENdpDq');
INSERT INTO users (login, password) VALUES ('editor2@throne.com', '$2a$10$QlYTH3iFqYFEp9OZWfL9ZuhGvCOlImZkSY8W8oY/yt3u434ENdpDq');
INSERT INTO users (login, password) VALUES ('user@throne.com', '$2a$10$QlYTH3iFqYFEp9OZWfL9ZuhGvCOlImZkSY8W8oY/yt3u434ENdpDq');
INSERT INTO users (login, password) VALUES ('user1@throne.com', '$2a$10$QlYTH3iFqYFEp9OZWfL9ZuhGvCOlImZkSY8W8oY/yt3u434ENdpDq');
INSERT INTO users (login, password) VALUES ('user2@throne.com', '$2a$10$QlYTH3iFqYFEp9OZWfL9ZuhGvCOlImZkSY8W8oY/yt3u434ENdpDq');
INSERT INTO users (login, password) VALUES ('user3@throne.com', '$2a$10$QlYTH3iFqYFEp9OZWfL9ZuhGvCOlImZkSY8W8oY/yt3u434ENdpDq');
INSERT INTO users (login, password) VALUES ('user4@throne.com', '$2a$10$QlYTH3iFqYFEp9OZWfL9ZuhGvCOlImZkSY8W8oY/yt3u434ENdpDq');

INSERT INTO roles (name) VALUES ('USER');
INSERT INTO roles (name) VALUES ('EDITOR');
INSERT INTO roles (name) VALUES ('ADMIN');

INSERT INTO users_roles (user_id, role_id) VALUES (1, 1);
INSERT INTO users_roles (user_id, role_id) VALUES (1, 2);
INSERT INTO users_roles (user_id, role_id) VALUES (1, 3);
INSERT INTO users_roles (user_id, role_id) VALUES (2, 1);
INSERT INTO users_roles (user_id, role_id) VALUES (2, 2);
INSERT INTO users_roles (user_id, role_id) VALUES (3, 1);
INSERT INTO users_roles (user_id, role_id) VALUES (3, 2);
INSERT INTO users_roles (user_id, role_id) VALUES (4, 1);
INSERT INTO users_roles (user_id, role_id) VALUES (5, 1);
INSERT INTO users_roles (user_id, role_id) VALUES (6, 1);
INSERT INTO users_roles (user_id, role_id) VALUES (7, 1);
INSERT INTO users_roles (user_id, role_id) VALUES (8, 1);

INSERT INTO comments (comment_date_time, comment, article_id, user_id) VALUES ('2019-12-23T01:38:49.368', 'АЫВаыаыаыаыа', 1, 1);
INSERT INTO comments (comment_date_time, comment, article_id, user_id) VALUES ('2019-12-23T01:38:49.368', 'АЫВаыаыаыаыа', 2, 2);
INSERT INTO comments (comment_date_time, comment, article_id, user_id) VALUES ('2019-12-23T01:38:49.368', 'АЫВаыаыаыаыа', 3, 3);
INSERT INTO comments (comment_date_time, comment, article_id, user_id) VALUES ('2019-12-23T01:38:49.368', 'АЫВаыаыаыаыа', 4, 4);
INSERT INTO comments (comment_date_time, comment, article_id, user_id) VALUES ('2019-12-23T01:38:49.368', 'АЫВаыаыаыаыа', 5, 5);
INSERT INTO comments (comment_date_time, comment, article_id, user_id) VALUES ('2019-12-23T01:38:49.368', 'АЫВаыаыаыаыа', 6, 6);
INSERT INTO comments (comment_date_time, comment, article_id, user_id) VALUES ('2019-12-23T01:38:49.368', 'АЫВаыаыаыаыа', 7, 7);
INSERT INTO comments (comment_date_time, comment, article_id, user_id) VALUES ('2019-12-23T01:38:49.368', 'АЫВаыаыаыаыа', 8, 8);