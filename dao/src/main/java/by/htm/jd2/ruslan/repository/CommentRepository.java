package by.htm.jd2.ruslan.repository;

import by.htm.jd2.ruslan.bean.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long> {
}
