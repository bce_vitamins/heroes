package by.htm.jd2.ruslan.repository;

import by.htm.jd2.ruslan.bean.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByLogin(String login);
}

