package by.htm.jd2.ruslan.repository;

import by.htm.jd2.ruslan.bean.entity.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleRepository extends JpaRepository<Article, Long> {
}
