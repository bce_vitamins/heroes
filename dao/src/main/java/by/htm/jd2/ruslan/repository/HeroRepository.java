package by.htm.jd2.ruslan.repository;

import by.htm.jd2.ruslan.bean.entity.Hero;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HeroRepository extends JpaRepository<Hero, Long> {
}
