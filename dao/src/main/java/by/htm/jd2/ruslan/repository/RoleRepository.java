package by.htm.jd2.ruslan.repository;

import by.htm.jd2.ruslan.bean.entity.Role;
import by.htm.jd2.ruslan.bean.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);
}

