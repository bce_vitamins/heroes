package by.htm.jd2.ruslan.repository;

import by.htm.jd2.ruslan.bean.entity.Battle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BattleRepository extends JpaRepository<Battle, Long> {
}
