package by.htm.jd2.ruslan.repository;

import by.htm.jd2.ruslan.bean.entity.Characteristic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CharacteristicRepository extends JpaRepository<Characteristic, Long> {
}
