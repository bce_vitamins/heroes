package by.htm.jd2.ruslan.repository;

import by.htm.jd2.ruslan.bean.entity.Role;
import by.htm.jd2.ruslan.bean.entity.User;
import by.htm.jd2.ruslan.config.TestDatabaseConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestDatabaseConfig.class)
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Test
    void findByLogin() {
        Role role = roleRepository.save(new Role(1L, "USER"));
        User user = new User(Collections.singletonList(role), "admtest@throne.com", "test", null);
        role.setUsers(Collections.singletonList(user));

        userRepository.save(user);
        User found = userRepository.findByLogin(user.getLogin());

        assertNotNull(found);
        assertEquals(found, user);
    }
}
