package by.htm.jd2.ruslan.repository;

import by.htm.jd2.ruslan.bean.entity.Role;
import by.htm.jd2.ruslan.config.TestDatabaseConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestDatabaseConfig.class)
public class RoleRepositoryTest {

    @Autowired
    private RoleRepository roleRepository;

    @Test
    void findByName() {
        Role role = roleRepository.save(new Role(1L, "USER"));

        Role found = roleRepository.findByName(role.getName());

        assertNotNull(found);
        assertEquals(found, role);
    }
}
