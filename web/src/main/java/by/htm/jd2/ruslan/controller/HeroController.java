package by.htm.jd2.ruslan.controller;

import by.htm.jd2.ruslan.bean.entity.Characteristic;
import by.htm.jd2.ruslan.bean.entity.Hero;
import by.htm.jd2.ruslan.service.BattleService;
import by.htm.jd2.ruslan.service.HeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class HeroController {

    private HeroService heroService;
    private BattleService battleService;

    @Autowired
    public HeroController(HeroService heroService, BattleService battleService) {
        this.heroService = heroService;
        this.battleService = battleService;
    }

    @GetMapping("/hero")
    public String getHeroInfoPage(@RequestParam Long id, HttpServletRequest request) {
        Hero hero = heroService.findById(id);
        request.setAttribute("hero", hero);
        return "hero/hero";
    }

    @GetMapping("/hero/create")
    public String createHeroGetMethod() {
        return "hero/create-hero";
    }

    @PostMapping("/hero/create")
    public String createHeroPostMethod(Hero hero, Characteristic characteristic) {
        characteristic.setId(hero.getId());
        characteristic.setHero(hero);
        hero.setCharacteristic(characteristic);
        heroService.save(hero);
        return "redirect: /hero/update?id=" + hero.getId();
    }

    @GetMapping("/hero/update")
    public String updateHeroGetMethod(@RequestParam Long id, HttpServletRequest request) {
        Hero hero = heroService.findById(id);
        request.setAttribute("hero", hero);
        return "hero/update-hero";
    }

    @PostMapping("/hero/update")
    public String updateHeroPostMethod(Hero hero, Characteristic characteristic) {
        characteristic.setId(hero.getId());
        characteristic.setHero(hero);
        hero.setCharacteristic(characteristic);
        heroService.save(hero);
        return "redirect: /hero?id=" + hero.getId();
    }

    @GetMapping("/hero/delete")
    public String deleteHero(@RequestParam Long id) {
        battleService.deleteById(id);
        heroService.deleteById(id);
        return "redirect: /heroes";
    }

    @GetMapping("/heroes")
    public String getHeroListPage(HttpServletRequest request) {
        request.setAttribute("heroes", heroService.findAll());
        return "hero/heroes";
    }
}