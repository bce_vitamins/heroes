package by.htm.jd2.ruslan.controller;

import by.htm.jd2.ruslan.bean.dto.article.ViewArticleBasicInfoDto;
import by.htm.jd2.ruslan.bean.entity.Article;
import by.htm.jd2.ruslan.bean.entity.Comment;
import by.htm.jd2.ruslan.service.ArticleService;
import by.htm.jd2.ruslan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ArticleController {

    private ArticleService articleService;
    private UserService userService;

    @Autowired
    public ArticleController(ArticleService articleService, UserService userService) {
        this.articleService = articleService;
        this.userService = userService;
    }

    @GetMapping("/article")
    public String getArticleInfoPage(@RequestParam Long id, HttpServletRequest request, Principal principal) {
        Article article = articleService.findById(id);
        request.setAttribute("article", articleService.findById(id));
        if (principal != null) {
            request.setAttribute("user", userService.findByLogin(principal.getName()));
        }
        List<Comment> sortedComments = article.getComments().stream()
                .sorted(Comparator.comparing(o -> LocalDateTime.parse(o.getDateTime())))
                .collect(Collectors.toList());
        article.setComments(sortedComments);
        return "article/article";
    }

    @GetMapping("/article/create")
    public String createArticleGetMethod() {
        return "article/create-article";
    }

    @PostMapping("/article/create")
    public String createArticlePostMethod(Article article) {
        article.setDateTime(LocalDateTime.now().toString());
        articleService.save(article);
        return "redirect: /article?id=" + article.getId();
    }

    @GetMapping("/article/update")
    public String updateArticleGetMethod(@RequestParam Long id, HttpServletRequest request) {
        Article article = articleService.findById(id);
        request.setAttribute("article", article);
        return "article/update-article";
    }

    @PostMapping("/article/update")
    public String updateArticlePostMethod(Article article) {
        return "redirect: /article?id=" + articleService.save(article);
    }

    @GetMapping("/article/delete")
    public String deleteArticle(@RequestParam Long id) {
        articleService.deleteById(id);
        return "redirect: /articles";
    }

    @GetMapping("/articles")
    public String getArticleListPage(HttpServletRequest request, @RequestParam(defaultValue = "1") Integer pageNumber) {
        Page<Article> searchResult = articleService.getPage(pageNumber >= 1 ? pageNumber - 1 : 0);
        List<ViewArticleBasicInfoDto> articles = searchResult.stream()
                .map(articleEntity -> new ViewArticleBasicInfoDto(articleEntity.getId(), articleEntity.getTitle(),
                        articleEntity.getForeword(), articleEntity.getDateTime()))
                .collect(Collectors.toList());
        request.setAttribute("articles", articles);
        request.setAttribute("totalPages", searchResult.getTotalPages());
        request.setAttribute("currentPage", pageNumber);
        return "article/articles";
    }
}
