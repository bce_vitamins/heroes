package by.htm.jd2.ruslan.utils;

import java.util.regex.Pattern;

public final class CredentialsValidator {

    private static final String LOGIN_VALIDATION_REGEX = "^\\w*\\@{1}\\D{3,}(\\.*)$";

    private CredentialsValidator() {
    }

    public static boolean matchRegex(String string) {
        return Pattern.compile(LOGIN_VALIDATION_REGEX).matcher(string).matches();
    }
}
