package by.htm.jd2.ruslan.controller;

import by.htm.jd2.ruslan.bean.dto.battle.ViewBattleBasicInfoDto;
import by.htm.jd2.ruslan.bean.entity.Battle;
import by.htm.jd2.ruslan.service.BattleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class BattleController {

    private BattleService battleService;

    @Autowired
    public BattleController(BattleService battleService) {
        this.battleService = battleService;
    }

    @ModelAttribute("battles")
    public List<ViewBattleBasicInfoDto> allBattles() {
        return battleService.findAll();
    }

    @GetMapping("/battle")
    public String getBattleInfoPage(@RequestParam Long id, HttpServletRequest request) {
        Battle battle = battleService.findById(id);
        request.setAttribute("battle", battle);
        return "battle/battle";
    }

    @GetMapping("/battle/create")
    public String createBattleGetMethod() {
        return "battle/create-battle";
    }

    @PostMapping("/battle/create")
    public String createBattlePostMethod(Battle battle) {
        battleService.save(battle);
        return "redirect: /battle?id=" + battle.getId();
    }

    @GetMapping("/battle/update")
    public String updateBattleGetMethod(@RequestParam Long id, HttpServletRequest request) {
        Battle battle = battleService.findById(id);
        request.setAttribute("battle", battle);
        return "battle/update-battle";
    }

    @PostMapping("/battle/update")
    public String updateBattlePostMethod(Battle battle) {
        battle.setHeroes(null);
        battleService.save(battle);
        return "redirect: /battle?id=" + battle.getId();
    }

    @GetMapping("/battle/delete")
    public String deleteBattle(@RequestParam Long id) {
        battleService.deleteById(id);
        return "redirect: /battles";
    }

    @GetMapping("/battles")
    public String getBattleListPage() {
        return "battle/battles";
    }
}
