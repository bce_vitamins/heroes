package by.htm.jd2.ruslan.controller;

import by.htm.jd2.ruslan.bean.dto.comment.CommentDto;
import by.htm.jd2.ruslan.bean.entity.Comment;
import by.htm.jd2.ruslan.service.ArticleService;
import by.htm.jd2.ruslan.service.CommentService;
import by.htm.jd2.ruslan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

@Controller
public class CommentController {

    private CommentService commentService;
    private UserService userService;
    private ArticleService articleService;

    @Autowired
    public CommentController(CommentService commentService, UserService userService, ArticleService articleService) {
        this.commentService = commentService;
        this.userService = userService;
        this.articleService = articleService;
    }

    @ModelAttribute("comments")
    public List<Comment> allComments() {
        return commentService.findAll();
    }

    @GetMapping("/comment")
    public String getCommentInfoPage(@RequestParam Long id, HttpServletRequest request) {
        request.setAttribute("comment", commentService.findById(id));
        return "comment/comment";
    }

    @PostMapping("/comment/create")
    public String createCommentPostMethod(CommentDto commentDto) {
        Comment comment = new Comment();
        Long articleId = commentDto.getArticleId();
        comment.setArticle(articleService.findById(articleId));
        comment.setUser(userService.findById(commentDto.getUserId()));
        comment.setComment(commentDto.getComment());
        comment.setDateTime(LocalDateTime.now().toString());
        commentService.save(comment);
        return "redirect: /article?id=" + articleId;
    }

    @GetMapping("/comment/update")
    public String updateCommentGetMethod(@RequestParam Long id, HttpServletRequest request) {
        Comment comment = commentService.findById(id);
        request.setAttribute("comment", comment);
        return "comment/update-comment";
    }

    @PostMapping("/comment/update")
    public String updateCommentPostMethod(Comment comment) {
        commentService.save(comment);
        return "redirect: /comment/comment?id=" + comment.getId();
    }

    @GetMapping("/comment/delete")
    public String deleteComment(@RequestParam Long id) {
        commentService.deleteById(id);
        return "redirect: /comment/comments";
    }

    @GetMapping("/comment/comments")
    public String getCommentListPage() {
        return "comment/comments";
    }
}