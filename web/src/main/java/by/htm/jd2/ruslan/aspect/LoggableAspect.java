package by.htm.jd2.ruslan.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

@Aspect
public class LoggableAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggableAspect.class);
    private static final String REPORTS_DIVIDER = "<--------------------------------------------------------->";

    @Pointcut("within(by.htm.jd2.ruslan.service..*)")
    public void services() {
    }

    @Around("services()")
    public Object getServiceLog(ProceedingJoinPoint joinPoint) {
        long start = System.currentTimeMillis();
        Object result = null;
        try {
            result = joinPoint.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        String reportStringBuilder = reportRowFormat("Service", joinPoint.getTarget().getClass().getSimpleName()) +
                reportRowFormat("Method", joinPoint.getSignature().getName()) +
                reportRowFormat("Args", Arrays.toString(joinPoint.getArgs())) +
                reportRowFormat("Result", result != null ? result.toString() : "[]") +
                reportRowFormat("Implementing time (ms)",
                        Long.toString(System.currentTimeMillis() - start));
        LOGGER.info(wrapReport(reportStringBuilder));
        return result;
    }

    private String wrapReport(String report) {
        return "\n" + report + REPORTS_DIVIDER;
    }

    private String reportRowFormat(String parameterName, String parameterValue) {
        return String.format("%s: %s\n", parameterName, parameterValue);
    }
}
