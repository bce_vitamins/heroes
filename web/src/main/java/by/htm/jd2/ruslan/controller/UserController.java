package by.htm.jd2.ruslan.controller;

import by.htm.jd2.ruslan.bean.entity.Role;
import by.htm.jd2.ruslan.bean.entity.User;
import by.htm.jd2.ruslan.service.CommentService;
import by.htm.jd2.ruslan.service.RoleService;
import by.htm.jd2.ruslan.service.UserService;
import by.htm.jd2.ruslan.utils.CredentialsValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

@Controller
public class UserController {

    private UserService userService;
    private RoleService roleService;
    private CommentService commentService;

    @Autowired
    public UserController(UserService userService, RoleService roleService, CommentService commentService) {
        this.userService = userService;
        this.roleService = roleService;
        this.commentService = commentService;
    }

    @ModelAttribute("roles")
    public List<Role> roles() {
        return roleService.findAll();
    }

    @GetMapping("/user")
    public String getUserInfoPage(@RequestParam Long id, HttpServletRequest request) {
        User user = userService.findById(id);
        request.setAttribute("user", user);
        return "user/user";
    }

    @GetMapping("/user/create")
    public String createUserGetMethod() {
        return "user/create-user";
    }

    @PostMapping("/user/create")
    public String createUserPostMethod(User user) {
        if (!CredentialsValidator.matchRegex(user.getLogin()) || userService.findByLogin(user.getLogin()) != null) {
            return "redirect: /user/create";
        }

        user.setRoles(Collections.singletonList(new Role(1L, "USER")));
        userService.save(user);
        return "redirect: /articles";
    }

    @PostMapping("/user/update")
    public String updateUserPostMethod(User user) {
        userService.save(user);
        return "redirect: /users";
    }

    @GetMapping("/user/delete")
    public String deleteUser(@RequestParam Long id) {
        commentService.deleteById(id);
        userService.deleteById(id);
        return "redirect: /users";
    }

    @GetMapping("/users")
    public String getUserListPage(HttpServletRequest request) {
        request.setAttribute("users", userService.findAll());
        return "user/users";
    }
}