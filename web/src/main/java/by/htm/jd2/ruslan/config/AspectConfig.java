package by.htm.jd2.ruslan.config;

import by.htm.jd2.ruslan.aspect.LoggableAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages = {"by.htm.jd2.ruslan.service"})
public class AspectConfig {

    @Bean
    public LoggableAspect audienceAspect() {
        return new LoggableAspect();
    }
}
