First, you see the login window, where you can enter the site or register by clicking registration. And select the 
locale, of course.
On the site you can:
- as a user: read articles and view a list of characters;
- as an editor: + edit articles and characters;
- as an administrator: + view users and edit them.