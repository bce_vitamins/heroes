package by.htm.jd2.ruslan.bean.dto.battle;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ViewBattleBasicInfoDto {

    private long id;
    private String title;
    private String dateTime;
}
