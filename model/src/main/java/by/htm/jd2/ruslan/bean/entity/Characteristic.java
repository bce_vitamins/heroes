package by.htm.jd2.ruslan.bean.entity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@ToString(callSuper = true, exclude = "hero")
@EqualsAndHashCode(callSuper = false, exclude = {"hero"})
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "characteristics")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "heroes")
public class Characteristic extends BaseEntity {

    @OneToOne(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "hero_id")
    private Hero hero;

    @Column(name = "health", nullable = false)
    private int health;

    @Column(name = "strength", nullable = false)
    private int strength;

    @Column(name = "agility", nullable = false)
    private int agility;

}
