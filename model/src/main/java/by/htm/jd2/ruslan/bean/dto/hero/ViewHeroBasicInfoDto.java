package by.htm.jd2.ruslan.bean.dto.hero;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ViewHeroBasicInfoDto {

    private long id;
    private String name;
    private String nickname;
}
