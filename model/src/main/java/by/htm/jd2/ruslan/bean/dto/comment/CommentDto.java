package by.htm.jd2.ruslan.bean.dto.comment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CommentDto {

    private Long userId;
    private Long articleId;
    private String comment;
}
