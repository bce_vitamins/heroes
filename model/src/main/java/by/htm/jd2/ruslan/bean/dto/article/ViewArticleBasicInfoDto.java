package by.htm.jd2.ruslan.bean.dto.article;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ViewArticleBasicInfoDto {

    private long id;
    private String title;
    private String foreword;
    private String dateTime;
}
