package by.htm.jd2.ruslan.bean.entity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@ToString(callSuper = true, exclude = "heroes")
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "battles")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "heroes")
public class Battle extends BaseEntity {

    @Column(name = "title")
    private String title;

    @Column(name = "description", nullable = false, columnDefinition = "TEXT")
    private String description;

    @Column(name = "date_time", nullable = false, columnDefinition = "TEXT")
    private String dateTime;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "participants",
            joinColumns = @JoinColumn(name = "battle_id"),
            inverseJoinColumns = @JoinColumn(name = "hero_id")
    )
    private Set<Hero> heroes = new HashSet<>();
}
