package by.htm.jd2.ruslan.service.impl;

import by.htm.jd2.ruslan.bean.dto.article.ViewArticleBasicInfoDto;
import by.htm.jd2.ruslan.bean.entity.Article;
import by.htm.jd2.ruslan.repository.ArticleRepository;
import by.htm.jd2.ruslan.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ArticleServiceImpl implements ArticleService {

    public static final int PAGE_SIZE = 3;
    private ArticleRepository articleRepository;

    @Autowired
    public ArticleServiceImpl(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public List<Article> getArticlesPage(Integer pageNumber, Integer pageSize) {
        return null;
    }

    @Override
    public Page<Article> getPage(Integer pageNumber) {
        PageRequest pageRequest = PageRequest.of(pageNumber, PAGE_SIZE);
        return articleRepository.findAll(pageRequest);
    }

    @Override
    public List<ViewArticleBasicInfoDto> findAll() {
        return articleRepository.findAll().stream()
                .map(articleEntity -> new ViewArticleBasicInfoDto(articleEntity.getId(), articleEntity.getTitle(),
                        articleEntity.getForeword(), articleEntity.getDateTime()))
                .collect(Collectors.toList());
    }

    @Override
    public Long save(Article article) {
        return articleRepository.save(article).getId();
    }

    @Override
    public Article findById(Long id) {
        return articleRepository.findById(id).get();
    }

    @Override
    public void delete(Article article) {
        articleRepository.delete(article);
    }

    @Override
    public void deleteById(Long id) {
        articleRepository.deleteById(id);
    }
}
