package by.htm.jd2.ruslan.service;

import by.htm.jd2.ruslan.bean.entity.Role;

import java.util.List;

public interface RoleService {

    List<Role> findAll();

    Role findByName(String name);
}
