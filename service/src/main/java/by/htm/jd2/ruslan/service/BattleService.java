package by.htm.jd2.ruslan.service;

import by.htm.jd2.ruslan.bean.dto.battle.ViewBattleBasicInfoDto;
import by.htm.jd2.ruslan.bean.entity.Battle;

import java.util.List;

public interface BattleService {

    List<ViewBattleBasicInfoDto> findAll();

    Long save(Battle battle);

    Battle findById(Long id);

    void delete(Battle battle);

    void deleteById(Long id);
}
