package by.htm.jd2.ruslan.service.impl;

import by.htm.jd2.ruslan.bean.dto.battle.ViewBattleBasicInfoDto;
import by.htm.jd2.ruslan.bean.entity.Battle;
import by.htm.jd2.ruslan.repository.BattleRepository;
import by.htm.jd2.ruslan.service.BattleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class BattleServiceImpl implements BattleService {

    private BattleRepository battleRepository;

    @Autowired
    public BattleServiceImpl(BattleRepository battleRepository) {
        this.battleRepository = battleRepository;
    }

    @Override
    public List<ViewBattleBasicInfoDto> findAll() {
        return battleRepository.findAll().stream()
                .map(battleEntity -> new ViewBattleBasicInfoDto(battleEntity.getId(), battleEntity.getTitle(),
                        battleEntity.getDateTime()))
                .collect(Collectors.toList());
    }

    @Override
    public Long save(Battle battle) {
        return battleRepository.save(battle).getId();
    }

    @Override
    public Battle findById(Long id) {
        return battleRepository.findById(id).get();
    }

    @Override
    public void delete(Battle battle) {
        battleRepository.delete(battle);
    }

    @Override
    public void deleteById(Long id) {
        battleRepository.deleteById(id);
    }
}
