package by.htm.jd2.ruslan.service;

import by.htm.jd2.ruslan.bean.dto.article.ViewArticleBasicInfoDto;
import by.htm.jd2.ruslan.bean.entity.Article;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ArticleService {

    List<Article> getArticlesPage(Integer pageNumber, Integer pageSize);

    Page<Article> getPage(Integer pageNumber);

    List<ViewArticleBasicInfoDto> findAll();

    Long save(Article article);

    Article findById(Long id);

    void delete(Article article);

    void deleteById(Long id);
}
