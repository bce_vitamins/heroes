package by.htm.jd2.ruslan.config;

import by.htm.jd2.ruslan.bean.config.CacheConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("by.htm.jd2.ruslan.service")
@Import({PersistenceConfig.class, CacheConfig.class})
public class ServiceConfig {
}