package by.htm.jd2.ruslan.service;

import by.htm.jd2.ruslan.bean.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    User findByLogin(String login);

    List<User> findAll();

    Long save(User user);

    User findById(Long id);

    void delete(User user);

    void deleteById(Long id);
}
