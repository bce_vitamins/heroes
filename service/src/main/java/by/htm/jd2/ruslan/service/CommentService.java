package by.htm.jd2.ruslan.service;

import by.htm.jd2.ruslan.bean.entity.Comment;

import java.util.List;

public interface CommentService {

    List<Comment> findAll();

    Long save(Comment comment);

    Comment findById(Long id);

    void delete(Comment comment);

    void deleteById(Long id);
}
