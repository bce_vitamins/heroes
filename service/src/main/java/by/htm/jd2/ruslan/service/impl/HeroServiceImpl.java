package by.htm.jd2.ruslan.service.impl;

import by.htm.jd2.ruslan.bean.dto.hero.ViewHeroBasicInfoDto;
import by.htm.jd2.ruslan.bean.entity.Hero;
import by.htm.jd2.ruslan.repository.HeroRepository;
import by.htm.jd2.ruslan.service.HeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class HeroServiceImpl implements HeroService {

    private HeroRepository heroRepository;

    @Autowired
    public HeroServiceImpl(HeroRepository heroRepository) {
        this.heroRepository = heroRepository;
    }

    public List<ViewHeroBasicInfoDto> findAll() {
        return heroRepository.findAll().stream()
                .map(heroEntity -> new ViewHeroBasicInfoDto(heroEntity.getId(), heroEntity.getName(),
                        heroEntity.getNickname()))
                .collect(Collectors.toList());
    }

    public Long save(Hero hero) {
        return heroRepository.save(hero).getId();
    }

    public Hero findById(Long id) {
        return heroRepository.findById(id).get();
    }

    @Override
    public void deleteById(Long id) {
        heroRepository.deleteById(id);
    }

    public void delete(Hero hero) {
        heroRepository.delete(hero);
    }
}
