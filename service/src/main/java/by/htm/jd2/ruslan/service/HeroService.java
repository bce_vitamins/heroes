package by.htm.jd2.ruslan.service;

import by.htm.jd2.ruslan.bean.dto.hero.ViewHeroBasicInfoDto;
import by.htm.jd2.ruslan.bean.entity.Hero;

import java.util.List;

public interface HeroService {

    List<ViewHeroBasicInfoDto> findAll();

    Long save(Hero hero);

    Hero findById(Long id);

    void delete(Hero hero);

    void deleteById(Long id);
}
