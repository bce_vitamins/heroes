package by.htm.jd2.ruslan.service.impl.service;

import by.htm.jd2.ruslan.bean.dto.hero.ViewHeroBasicInfoDto;
import by.htm.jd2.ruslan.bean.entity.Hero;
import by.htm.jd2.ruslan.repository.HeroRepository;
import by.htm.jd2.ruslan.service.impl.HeroServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class HeroServiceTest extends BaseServiceTest {

    @Mock
    protected HeroRepository heroRepository;

    @InjectMocks
    protected HeroServiceImpl heroService;

    @Test
    void findAll() {
        List<Hero> heroesList = new ArrayList<>();
        firstHero.setId(1L);
        secondHero.setId(2L);
        heroesList.add(firstHero);
        heroesList.add(secondHero);

        when(heroRepository.findAll()).thenReturn(heroesList);

        List<ViewHeroBasicInfoDto> foundedHeroes = heroService.findAll();

        assertNotNull(foundedHeroes);
        assertEquals(foundedHeroes.size(), heroesList.size());
        assertEquals(foundedHeroes.get(0).getId(), heroesList.get(0).getId());
        assertEquals(foundedHeroes.get(1).getId(), heroesList.get(1).getId());
    }

    @Test
    void save() {
        when(heroRepository.save(any(Hero.class))).thenReturn(firstHero);
        Long heroLong = heroService.save(firstHero);

        assertNotNull(heroLong);
        assertEquals((long) heroLong, 1L);
    }

    @Test
    void findById() {
        when(heroRepository.findById(1L)).thenReturn(Optional.ofNullable(firstHero));

        Hero hero = heroService.findById(1L);

        assertNotNull(hero);
        assertEquals(hero.getName(), firstHero.getName());
        assertEquals(hero.getNickname(), firstHero.getNickname());
        assertEquals(hero.getAge(), firstHero.getAge());
        assertEquals(hero.getGender(), firstHero.getGender());
        assertEquals(hero.getDescription(), firstHero.getDescription());
        assertEquals(hero.getCharacteristic(), firstHero.getCharacteristic());
        assertEquals(hero.getBattles(), firstHero.getBattles());
    }

    @Test
    void delete() {
        doNothing().when(heroRepository).delete(firstHero);
        heroService.delete(firstHero);
        verify(heroRepository, times(1)).delete(firstHero);
    }

    @Test
    void deleteById() {
        doNothing().when(heroRepository).deleteById(1L);
        heroService.deleteById(1L);
        verify(heroRepository, times(1)).deleteById(1L);
    }
}
