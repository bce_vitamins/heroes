package by.htm.jd2.ruslan.service.impl.service;

import by.htm.jd2.ruslan.bean.entity.Article;
import by.htm.jd2.ruslan.bean.entity.Battle;
import by.htm.jd2.ruslan.bean.entity.Characteristic;
import by.htm.jd2.ruslan.bean.entity.Comment;
import by.htm.jd2.ruslan.bean.entity.Hero;
import by.htm.jd2.ruslan.bean.entity.Role;
import by.htm.jd2.ruslan.bean.entity.User;
import by.htm.jd2.ruslan.bean.entity.enums.Gender;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BaseServiceTest {

    protected static List<Role> roles = Collections.singletonList(new Role(1L, "USER"));

    protected static Article firstArticle = new Article("1", "1", "1", "2019-09-30 20:48:12", null);
    protected static Article secondArticle = new Article("2", "2", "2", "2019-09-30 20:48:12", null);

    protected static Battle firstBattle = new Battle("1", "2", "2019-09-30 20:48:12", null);
    protected static Battle secondBattle = new Battle("1", "2", "2019-09-30 20:48:12", null);

    private static Set<Battle> battles = new HashSet<>();

    protected static Comment firstComment = new Comment(null, null, "1", "2019-09-30 20:48:12");
    protected static Comment secondComment = new Comment(null, null, "2", "2019-09-30 20:48:12");

    protected static Characteristic firstCharacteristic = new Characteristic(null, 1, 1, 1);
    protected static Characteristic secondCharacteristic = new Characteristic(null, 2, 1, 1);

    protected static Hero firstHero = new Hero("1", "Wolf", 26, Gender.MALE, "asdasd",
            firstCharacteristic, battles);
    protected static Hero secondHero = new Hero("2", "Wolf", 26, Gender.MALE, "asdasd",
            secondCharacteristic, battles);

    protected static User firstUser = new User(roles, "admtest@throne.com", "test", null);
    protected static User secondUser = new User(roles, "12345@throne.com", "test", null);

    protected static Role firstRole = new Role(2L, "ADMIN");
    protected static Role secondRole = new Role(3L, "EDITOR");

    static {
        battles.add(firstBattle);
        battles.add(secondBattle);

        firstArticle.setId(1L);
        secondArticle.setId(2L);

        firstBattle.setId(1L);
        secondBattle.setId(2L);

        firstComment.setId(1L);
        secondComment.setId(2L);

        firstHero.setId(1L);
        secondHero.setId(2L);

        firstUser.setId(1L);
        secondUser.setId(2L);
    }
}
