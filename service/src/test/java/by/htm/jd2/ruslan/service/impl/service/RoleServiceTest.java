package by.htm.jd2.ruslan.service.impl.service;

import by.htm.jd2.ruslan.bean.entity.Role;
import by.htm.jd2.ruslan.repository.RoleRepository;
import by.htm.jd2.ruslan.service.impl.RoleServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RoleServiceTest extends BaseServiceTest {

    @Mock
    protected RoleRepository roleRepository;

    @InjectMocks
    protected RoleServiceImpl roleService;

    @Test
    void findByName() {
        when(roleRepository.findByName(any())).thenReturn(firstRole);
        Role role = roleService.findByName("ADMIN");

        assertNotNull(role);
        assertEquals(role, firstRole);
    }

    @Test
    void findAll() {
        List<Role> roleList = new ArrayList<>();
        firstUser.setId(1L);
        secondUser.setId(2L);
        roleList.add(firstRole);
        roleList.add(secondRole);

        when(roleRepository.findAll()).thenReturn(roleList);

        List<Role> foundedRoles = roleService.findAll();

        assertNotNull(foundedRoles);
        assertEquals(foundedRoles.size(), roleList.size());
        assertEquals(foundedRoles.get(0).getId(), roleList.get(0).getId());
        assertEquals(foundedRoles.get(1).getId(), roleList.get(1).getId());
    }
}
