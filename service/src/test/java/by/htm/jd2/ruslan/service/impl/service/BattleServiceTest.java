package by.htm.jd2.ruslan.service.impl.service;

import by.htm.jd2.ruslan.bean.dto.battle.ViewBattleBasicInfoDto;
import by.htm.jd2.ruslan.bean.entity.Battle;
import by.htm.jd2.ruslan.repository.BattleRepository;
import by.htm.jd2.ruslan.service.impl.BattleServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BattleServiceTest extends BaseServiceTest {

    @Mock
    protected BattleRepository battleRepository;

    @InjectMocks
    protected BattleServiceImpl battleService;

    @Test
    void findAll() {
        List<Battle> battlesList = new ArrayList<>();
        firstBattle.setId(1L);
        secondBattle.setId(2L);
        battlesList.add(firstBattle);
        battlesList.add(secondBattle);

        when(battleRepository.findAll()).thenReturn(battlesList);

        List<ViewBattleBasicInfoDto> foundedBattles = battleService.findAll();

        assertNotNull(foundedBattles);
        assertEquals(foundedBattles.size(), battlesList.size());
        assertEquals(foundedBattles.get(0).getId(), battlesList.get(0).getId());
        assertEquals(foundedBattles.get(1).getId(), battlesList.get(1).getId());
    }

    @Test
    void save() {
        when(battleRepository.save(any(Battle.class))).thenReturn(firstBattle);
        Long battleLong = battleService.save(firstBattle);

        assertNotNull(battleLong);
        assertEquals((long) battleLong, 1L);
    }

    @Test
    void findById() {
        when(battleRepository.findById(1L)).thenReturn(Optional.ofNullable(firstBattle));
        Battle fullInfoFirstBattle = battleService.findById(1L);

        assertNotNull(fullInfoFirstBattle);
        assertEquals(fullInfoFirstBattle.getTitle(), firstBattle.getTitle());
        assertEquals(fullInfoFirstBattle.getDescription(), firstBattle.getDescription());
        assertEquals(fullInfoFirstBattle.getDateTime(), firstBattle.getDateTime());
        assertEquals(fullInfoFirstBattle.getId(), firstBattle.getId());
        assertEquals(fullInfoFirstBattle.getHeroes(), firstBattle.getHeroes());
    }

    @Test
    void delete() {
        doNothing().when(battleRepository).delete(firstBattle);
        battleService.delete(firstBattle);
        verify(battleRepository, times(1)).delete(firstBattle);
    }

    @Test
    void deleteById() {
        doNothing().when(battleRepository).deleteById(1L);
        battleService.deleteById(1L);
        verify(battleRepository, times(1)).deleteById(1L);
    }
}
