package by.htm.jd2.ruslan.service.impl.config;

import by.htm.jd2.ruslan.config.ServiceConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(ServiceConfig.class)
public class TestConfig {
}
