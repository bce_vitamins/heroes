package by.htm.jd2.ruslan.service.impl.service;

import by.htm.jd2.ruslan.bean.entity.Comment;
import by.htm.jd2.ruslan.repository.CommentRepository;
import by.htm.jd2.ruslan.service.impl.CommentServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CommentServiceTest extends BaseServiceTest {

    @Mock
    protected CommentRepository commentRepository;

    @InjectMocks
    protected CommentServiceImpl commentService;

    @Test
    void findAll() {
        List<Comment> commentsList = new ArrayList<>();
        firstComment.setId(1L);
        secondComment.setId(2L);
        commentsList.add(firstComment);
        commentsList.add(secondComment);

        when(commentRepository.findAll()).thenReturn(commentsList);

        List<Comment> foundedComments = commentService.findAll();

        assertNotNull(foundedComments);
        assertEquals(foundedComments.size(), commentsList.size());
        assertEquals(foundedComments.get(0).getId(), commentsList.get(0).getId());
        assertEquals(foundedComments.get(1).getId(), commentsList.get(1).getId());
    }

    @Test
    void save() {
        when(commentRepository.save(any(Comment.class))).thenReturn(firstComment);
        Long commentLong = commentService.save(firstComment);

        assertNotNull(commentLong);
        assertEquals((long) commentLong, 1L);
    }


    @Test
    void findById() {
        when(commentRepository.findById(1L)).thenReturn(Optional.of(firstComment));

        Comment fullInfoFirstComment = commentService.findById(1L);

        assertNotNull(fullInfoFirstComment);
        assertEquals(fullInfoFirstComment.getComment(), firstComment.getComment());
        assertEquals(fullInfoFirstComment.getDateTime(), firstComment.getDateTime());
        assertEquals(fullInfoFirstComment.getArticle(), firstComment.getArticle());
        assertEquals(fullInfoFirstComment.getUser(), firstComment.getUser());
        assertEquals(fullInfoFirstComment.getId(), firstComment.getId());
    }

    @Test
    void delete() {
        doNothing().when(commentRepository).delete(firstComment);
        commentService.delete(firstComment);
        verify(commentRepository, times(1)).delete(firstComment);
    }

    @Test
    void deleteById() {
        doNothing().when(commentRepository).deleteById(1L);
        commentService.deleteById(1L);
        verify(commentRepository, times(1)).deleteById(1L);
    }
}
