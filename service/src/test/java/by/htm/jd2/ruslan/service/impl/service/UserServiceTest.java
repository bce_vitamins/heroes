package by.htm.jd2.ruslan.service.impl.service;

import by.htm.jd2.ruslan.bean.entity.User;
import by.htm.jd2.ruslan.repository.UserRepository;
import by.htm.jd2.ruslan.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest extends BaseServiceTest {

    @Mock
    protected UserRepository userRepository;

    @InjectMocks
    protected UserServiceImpl userService;

    @Test
    void findByLogin() {
        when(userRepository.findByLogin(any())).thenReturn(secondUser);
        User user = userService.findByLogin("aaa@throne.com");

        assertNotNull(user);
        assertEquals(user, secondUser);
    }

    @Test
    void findAll() {
        List<User> usersList = new ArrayList<>();
        firstUser.setId(1L);
        secondUser.setId(2L);
        usersList.add(firstUser);
        usersList.add(secondUser);

        when(userRepository.findAll()).thenReturn(usersList);

        List<User> foundedUsers = userService.findAll();

        assertNotNull(foundedUsers);
        assertEquals(foundedUsers.size(), usersList.size());
        assertEquals(foundedUsers.get(0).getId(), usersList.get(0).getId());
        assertEquals(foundedUsers.get(1).getId(), usersList.get(1).getId());
    }

    @Test
    void save() {
        when(userRepository.save(any(User.class))).thenReturn(firstUser);
        Long userLong = userService.save(firstUser);

        assertNotNull(userLong);
        assertEquals((long) userLong, 1L);
    }

    @Test
    void findById() {
        when(userRepository.findById(1L)).thenReturn(Optional.of(firstUser));

        firstUser.setId(1L);
        User fullInfoFirstUser = userService.findById(1L);

        assertNotNull(fullInfoFirstUser);
        assertEquals(fullInfoFirstUser.getLogin(), firstUser.getLogin());
        assertEquals(fullInfoFirstUser.getPassword(), firstUser.getPassword());
    }

    @Test
    void delete() {
        doNothing().when(userRepository).delete(firstUser);
        userService.delete(firstUser);
        verify(userRepository, times(1)).delete(firstUser);
    }

    @Test
    void deleteById() {
        doNothing().when(userRepository).deleteById(1L);
        userService.deleteById(1L);
        verify(userRepository, times(1)).deleteById(1L);
    }
}
