package by.htm.jd2.ruslan.service.impl.service;

import by.htm.jd2.ruslan.bean.dto.article.ViewArticleBasicInfoDto;
import by.htm.jd2.ruslan.bean.entity.Article;
import by.htm.jd2.ruslan.repository.ArticleRepository;
import by.htm.jd2.ruslan.service.impl.ArticleServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ArticleServiceTest extends BaseServiceTest {

    @Mock
    private ArticleRepository articleRepository;

    @InjectMocks
    private ArticleServiceImpl articleService;

    @Test
    void findAll() {
        List<Article> articlesList = new ArrayList<>();

        articlesList.add(firstArticle);
        articlesList.add(secondArticle);

        when(articleRepository.findAll()).thenReturn(articlesList);

        List<ViewArticleBasicInfoDto> foundedArticles = articleService.findAll();

        assertNotNull(foundedArticles);
        assertEquals(foundedArticles.size(), articlesList.size());
        assertEquals(foundedArticles.get(0).getId(), articlesList.get(0).getId());
        assertEquals(foundedArticles.get(1).getId(), articlesList.get(1).getId());
    }

    @Test
    void save() {
        when(articleRepository.save(any(Article.class))).thenReturn(firstArticle);

        Long articleLong = articleService.save(firstArticle);

        assertNotNull(articleLong);
        assertEquals((long) articleLong, 1L);
    }

    @Test
    void findById() {
        when(articleRepository.findById(1L)).thenReturn(Optional.of(firstArticle));

        firstArticle.setId(1L);
        secondArticle.setId(2L);

        Article fullInfoFirstArticle = articleService.findById(1L);

        fullInfoFirstArticle.setId(1L);

        assertNotNull(fullInfoFirstArticle);
        assertEquals(fullInfoFirstArticle.getForeword(), firstArticle.getForeword());
        assertEquals(fullInfoFirstArticle.getTitle(), firstArticle.getTitle());
        assertEquals(fullInfoFirstArticle.getDescription(), firstArticle.getDescription());
        assertEquals(fullInfoFirstArticle.getDateTime(), firstArticle.getDateTime());
        assertEquals(fullInfoFirstArticle.getComments(), firstArticle.getComments());
    }

    @Test
    void delete() {
        doNothing().when(articleRepository).delete(firstArticle);
        articleService.delete(firstArticle);
        verify(articleRepository, times(1)).delete(firstArticle);
    }

    @Test
    void deleteById() {
        doNothing().when(articleRepository).deleteById(1L);
        articleService.deleteById(1L);
        verify(articleRepository, times(1)).deleteById(1L);
    }
}
